# SVG Tools Collection

This repository collects various tools to modify and create SVG files that could be useful for the Inkscape community.

To clone this repository, including its submodules, also updating each submodule to its latest version, do: 

`git clone --recurse-submodules https://gitlab.com/Moini/ink_extensions.git`

Each included third-party repository has its own directory structure, and some have dependencies that are not included. Be sure to read the individual project's documentation.

Please note each project's license before you use, edit or redistribute it.

If you have an SVG tool that you would like to see added here, would like to expand a tool's description in this README file, or would like to otherwise see this repository updated, please [open an issue](https://gitlab.com/Moini/svg_tools/-/issues) for it.


## Contents


### SVGnest

**Author:**  
**Description:**  
**URL:**  
**Inkscape versions:**  
**Operating systems:**  
**Menu location:**  
**Last tested:**  (commit hash, date) by (tester)  
**License:**  
Screenshot

---

### svgo

---
